ruleset wovyn_base {
  meta {
    shares __testing
    use module lab2_keys
    use module twilio
        with account_sid = keys:twilio_keys{"account_sid"}
            auth_token =  keys:twilio_keys{"auth_token"}
    use module sensor_profile
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      ] , "events": [
        {"domain": "wovyn", "type": "threshold_violation",
          "attrs": ["temperature"]}
      ]
    }

    phone_from = "+13853233201"
  }

  rule process_heartbeat {
    select when wovyn heartbeat where event:attr("genericThing")
    pre {
      genericThing = event:attr("genericThing")
      temperature = genericThing{"data"}{"temperature"}
    }
    fired {
      raise wovyn event "new_temperature_reading"
        attributes {"temperature": temperature, "timestamp": time:now()}
    }
  }

  rule temperature_reading {
    select when wovyn new_temperature_reading
    pre {
      never_used = event:attrs.klog("attrs")
    }
  }

  rule find_high_temps {
    select when wovyn new_temperature_reading
    pre {
      temperature = event:attr("temperature")[0]{"temperatureF"}.klog("fahrenheit")
      temperatureThreshold = sensor_profile:threshold().klog("violation threshold:")
    }
    if temperature > temperatureThreshold then
      send_directive("temperature violation")
    fired {
      raise wovyn event "threshold_violation"
        attributes {"temperature": temperature, "timestamp": event:attr("timestamp")}
    }
  }

  rule threshold_notification {
    select when wovyn threshold_violation
    pre {
      phone_to_value = sensor_profile:sms().klog("sms contact:")
    }

    twilio:send_sms(phone_to_value, phone_from,
      "high temperature violation: " + event:attr("temperature") + " over threshold: " + sensor_profile:threshold()
    )
  }
}
