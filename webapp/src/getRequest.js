export function getRequest(url, callback) {
  // console.log("Submitting request to: " + url);
  fetch(url)
  .then(data => {
    if (data.ok) {
      return data.json();
    } else {
      throw new Error("Bad response: " + data.status);
    }
  })
  .then(data => {
    if (callback) {
      callback(data);
    }
  })
  .catch(error => {
    console.log(error);
  });
};
