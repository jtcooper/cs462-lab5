import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';

import {Home} from './home';
import {Profile} from './profile';
import './index.css';

class App extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<div>
					<Route exact path={"/"} component={Home}/>
					<Route path={"/profile"} component={Profile}/>
				</div>
			</BrowserRouter>
		);
	}
}

ReactDOM.render(<App />, document.getElementById('root'));
