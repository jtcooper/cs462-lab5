import React from 'react';
import {getRequest} from "./getRequest";

const UPDATE_INTERVAL = 30000;

export class Home extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentTemperature: null,
			recentTemps: [],
			violations: [],
			updateTimer: null
		};
	}

	componentDidMount() {
		this.updateTemperatures();
	};

	profileClick = () => {
		// clear the timer so we don't keep issuing requests
		clearTimeout(this.state.updateTimer);
		this.props.history.push('/profile');
	};

	updateTemperatures = () => {
		// send requests to picos to get updated temperatures
		console.log("Refreshing temperatures...");
		let read_temps_url = "http://localhost:8080/sky/cloud/JfJLQpyBRgJd1wBnDJPBaE/temperature_store/temperatures";
		let temps_callback = (data) => {
			data.sort(this.temperatureSort);
			if (data.length > 10) {
				data = data.slice(0, 10);
			}
			this.setState({
				currentTemperature: data[0],
				recentTemps: data
			});
		};
		getRequest(read_temps_url, temps_callback);

		let violations_url = "http://localhost:8080/sky/cloud/JfJLQpyBRgJd1wBnDJPBaE/temperature_store/threshold_violations";
		let violations_callback = (data) => {
			data.sort(this.temperatureSort);
			this.setState({
				violations: data
			});
		}
		getRequest(violations_url, violations_callback);
		// set timer for next update
		this.setState({
			updateTimer: setTimeout(this.updateTemperatures, UPDATE_INTERVAL)
		});
	};

	temperatureSort = (a, b) => {
		// sorting function by timestamp
		return new Date(b.timestamp) - new Date(a.timestamp);
	};

	render() {
		const current_reading = this.state.currentTemperature ?
			this.state.currentTemperature.temperature :
			"-";
		const temps = this.state.recentTemps.map((temp) =>
			<li key={temp.timestamp}>{temp.temperature}F -- {new Date(temp.timestamp).toString()}</li>
		);
		const violations = this.state.violations.map((temp) =>
			<li key={temp.timestamp}>{temp.temperature}F -- {new Date(temp.timestamp).toString()}</li>
		);
		return (
			<div>
				<h1>Home</h1>
				<button onClick={this.profileClick}>Profile</button>
				<p>Current Temperature: {current_reading}F</p>
				<h3>Recent Temperatures</h3>
				<ul>{temps}</ul>
				<h3>Violations</h3>
				<ul>{violations}</ul>
			</div>
		);
	}
}
