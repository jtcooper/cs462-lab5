import React from 'react';
import {getRequest} from "./getRequest";

export class Profile extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			name: null,
			name_field: "",
			location: null,
			location_field: "",
			threshold: null,
			threshold_field: "",
			sms: null,
			sms_field: ""
		};
	}

  componentDidMount() {
    this.loadProfile();
  }

	handleSubmit = (event) => {
		event.preventDefault();
		this.handleClick();
	};

	addIfNotEmpty = (valueMap, field, value) => {
		if (value && value.length > 0) {
			valueMap[field] = value;
		}
	};

	handleClick = () => {
		// submit request to server to change profile
		let requestBody = {};
		this.addIfNotEmpty(requestBody, "name", this.state.name_field);
		this.addIfNotEmpty(requestBody, "location", this.state.location_field);
		this.addIfNotEmpty(requestBody, "threshold", this.state.threshold_field);
		this.addIfNotEmpty(requestBody, "sms", this.state.sms_field);
		let url = "http://localhost:8080/sky/event/JfJLQpyBRgJd1wBnDJPBaE/1/sensor/profile_updated";
		fetch(url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(requestBody)
		})
		.then(data => {
			// if profile updated successfully, request profile data again
			if (data.ok) {
				setTimeout(this.loadProfile, 100); // delay 100 ms to give the server time to update
				return data.json();
			}
		})
		.then(data => {
			// we log the post response just to check that it updated successfully
			console.log(data);
		});
	};

	handleTextChange = (event) => {
		switch (event.target.name) {
			case "name":
				this.setState({name_field: event.target.value});
				break;
			case "location":
				this.setState({location_field: event.target.value});
				break;
			case "threshold":
				this.setState({threshold_field: event.target.value});
				break;
			case "sms":
				this.setState({sms_field: event.target.value});
				break;
			default:
				break;
		}
	};

	loadProfile = () => {
    // load profile data
    console.log("Loading profile...");
		let url = "http://localhost:8080/sky/cloud/JfJLQpyBRgJd1wBnDJPBaE/sensor_profile/retrieve_profile";
		let callback = (data) => {
			this.setState({
				name: data.name,
				name_field: "",
				location: data.location,
				location_field: "",
				threshold: data.threshold,
				threshold_field: "",
				sms: data.sms,
				sms_field: ""
			});
		};
		getRequest(url, callback);
	};

  render() {
    return (
      <div>
        <h1>Profile</h1>
				<form onSubmit={this.handleSubmit}>
					<p><b>Name:</b> {this.state.name}</p>
					<input
							name="name"
							type="text"
							value={this.state.name_field}
							onChange={this.handleTextChange}
							placeholder="Name..."/>
					<p><b>Location:</b> {this.state.location}</p>
					<input
							name="location"
							type="text"
							value={this.state.location_field}
							onChange={this.handleTextChange}
							placeholder="Location..."/>
					<p><b>Violation threshold:</b> {this.state.threshold}</p>
					<input
							name="threshold"
							type="text"
							value={this.state.threshold_field}
							onChange={this.handleTextChange}
							placeholder="Violation threshold..."/>
					<p><b>SMS Notifications:</b> {this.state.sms}</p>
					<input
							name="sms"
							type="text"
							value={this.state.sms_field}
							onChange={this.handleTextChange}
							placeholder="SMS Contact..."/>
				</form>
				<br/>
				<button onClick={this.handleClick}>Update profile</button>
      </div>
    );
  }
}
